from abc import ABC

# Person abstract class
# Create a Person class that is an abstract class that has the following methods:
# getFullName method
# addRequest method
# checkRequest method
# addUser method

class Person():
	def getFullName():
		pass

	def addRequest():
		pass

	def checkRequest():
		pass

	def addUser():
		pass

################EMPLOYEE#########################
# Create an Employee class from Person with the following properties and methods:
# Properties (Make sure they are private and have getters/setters)
# firstName
# lastName
# email
# department
# Methods
# Abstract methods (All methods just return Strings of simple text)
# checkRequest() - placeholder method
# addUser() - placeholder method
# login() - outputs "<Email> has logged in"
# logout() - outputs "<Email> has logged out"

class Employee(Person):
	def __init__(self, firstName, lastName, email, department):
		super().__init__()

		self._firstName = firstName
		self._lastName = lastName
		self._email = email
		self._department = department

	def get_firstName(self):
		print(f'{self._firstName}')

	def set_firstName(self, firstName):
		self._firstName = firstName

	def get_lastName(self):
		print(f'{self._lastName}')

	def set_lastName(self, lastName):
		self._lastName = lastName

	def get_email(self):
		print(f'{self._email}')

	def set_email(self, email):
		self._email = email

	def get_department(self):
		print(f'{self._department}')

	def set_department(self, department):
		self._department = department

	def getFullName(self):
		return(f'{self._firstName} {self._lastName}')

	def checkRequest(self):
		print(f'Checking request...')

	def addRequest(self):
		return(f'Request has been added')

	def login(self):
		return(f'{self._email} has logged in')

	def logout(self):
		return(f'{self._email} has logged out')


###################Team Lead##########################
# Create a TeamLead class from Person with the following properties and methods:
# Properties (Make sure they are private and have getters/setters)
# firstName
# lastName
# email
# department
# Methods
# Abstract methods (All methods just return Strings of simple text)
# checkRequest() - placeholder method
# addUser() - placeholder method
# login() - outputs "<Email> has logged in"
# logout() - outputs "<Email> has logged out"
# addMember() - adds an employee to the members list

class TeamLead(Person):
	def __init__(self, firstName, lastName, email, department):
		super().__init__()

		self._firstName = firstName
		self._lastName = lastName
		self._email = email
		self._department = department
		self._members = []

	def get_firstName(self):
		print(f'{self._firstName}')

	def set_firstName(self, firstName):
		self._firstName = firstName

	def get_lastName(self):
		print(f'{self._lastName}')

	def set_lastName(self, lastName):
		self._lastName = lastName

	def get_email(self):
		print(f'{self._email}')

	def set_email(self, email):
		self._email = email

	def get_department(self):
		print(f'{self._department}')

	def set_department(self, department):
		self._department = department

	def getFullName(self):
		return(f'{self._firstName} {self._lastName}')

	def checkRequest(self):
		print(f'Checking request...')

	def addRequest(self):
		return(f'Request has been added!')

	def login(self):
		return(f'{self._email} has logged in')

	def logout(self):
		return(f'{self._email} has logged out')

	def addMember(self, member):
		#return(f'{self._firstName} {self._lastName}')
		self._members.append(member)

	def get_members(self):
		return self._members

#####################ADMIN###########################
# Create an Admin class from Person with the following properties and methods:
# Properties(make sure they are private and have getters/setters)
# firstName
# lastName
# email
# Department 
# Methods
# Abstract methods (All methods just return Strings of simple text)
# checkRequest() - placeholder method
# addRequest() - placeholder method
# login() - outputs "<Email> has logged in"
# logout() - outputs "<Email> has logged out"
# addUser() - outputs "New user added”

class Admin(Person):
	def __init__(self, firstName, lastName, email, department):
		super().__init__()

		self._firstName = firstName
		self._lastName = lastName
		self._email = email
		self._department = department

	def get_firstName(self):
		print(f'{self._firstName}')

	def set_firstName(self, firstName):
		self._firstName = firstName

	def get_lastName(self):
		print(f'{self._lastName}')

	def set_lastName(self, lastName):
		self._lastName = lastName

	def get_email(self):
		print(f'{self._email}')

	def set_email(self, email):
		self._email = email

	def get_department(self):
		print(f'{self._department}')

	def set_department(self, department):
		self._department = department

	def getFullName(self):
		return(f'{self._firstName} {self._lastName}')

	def checkRequest(self):
		print(f'Checking request...')

	def addUser(self):
		return(f'User has been added')

	def login(self):
		return(f'{self._email} has logged in')

	def logout(self):
		return(f'{self._email} has logged out')

	def addRequest(self):
		return(f'Request has been added')

#######################Request##########################
# Request class
# Create a Request class that has the following properties and methods:
# Properties
# name
# requester
# dateRequested
# status
# Methods
# updateRequest
# closeRequest
# cancelRequest

class Request():
	def __init__(self, name, requester, dateRequested):
		self.name = name
		self.requester = requester
		self.dateRequested = dateRequested
		self.status = "open"

	def updateRequest(self):
		print(f'Request {self.name} has been added')

	def get_status(self):
		print(f'{self.status}')

	def set_status(self, status):
		self.status = status

	def closeRequest(self):
		print(f'Request {self.name} has been closed')

	def cancelRequest(self):
		print(f'Request {self.name} has been cancelled')


#Test Cases
employee1 = Employee("John", "Doe", "djohn@mail.com", "Marketing")
employee2 = Employee("Jane", "Smith", "sjane@mail.com", "Marketing")
employee3 = Employee("Robert", "Patterson", "probert@mail.com", "Sales")
employee4 = Employee("Brandon", "Smith", "sbrandon@mail.com", "Sales")
admin1 = Admin("Monika", "Justin", "jmonika@mail.com", "Marketing")
teamLead1 = TeamLead("Michael", "Specter", "smichael@mail.com", "Sales")
req1 = Request("New hire orientation", teamLead1, "27-Jul-2021")
req2 = Request("Laptop repair", employee1, "1-Jul-2021")

assert employee1.getFullName() == "John Doe", "Full name should be John Doe"
assert admin1.getFullName() == "Monika Justin", "Full name should be Monika Justin"
assert teamLead1.getFullName() == "Michael Specter", "Full name should be Michael Specter"
assert employee2.login() == "sjane@mail.com has logged in"
assert employee2.addRequest() == "Request has been added"
assert employee2.logout() == "sjane@mail.com has logged out"

teamLead1.addMember(employee3)
teamLead1.addMember(employee4)
for indiv_emp in teamLead1.get_members():
    print(indiv_emp.getFullName())

assert admin1.addUser() == "User has been added"

req2.set_status("closed")
print(req2.closeRequest())
